# Installing on Arch Linux

Initial info:
- https://stackoverflow.com/questions/66408814/running-c-jupiter-notebook-in-vscode-insiders
- https://aur.archlinux.org/packages/xeus-cling/

```sh
# apparently xeus-cling needs some build output from the cling build which yay removes
# do a manual build
yay -S cling
tar -x cling*
makepkg -s

yay xeus-cling
```

No conda installation is necessary.

At the time of writing, you can simply open VSCode, start a new notebook (Ctrl+Shift+P > Jupyter: Create New...), and select the xeus-cling kernel (should be autodetected).