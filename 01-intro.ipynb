{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Intro\n",
    "\n",
    "The C++ Annotations Intro section is essentially an eclectic mix of various differences between C and C++, so the topics in this chapter are not covered in its full breadth.\n",
    "\n",
    "It's just to remind the reader that, while C++ seems like a direct superset of C, its designers have allowed themselves some freedom in reworking some of its concepts (usually by introducing stricter definitions) in order to reduce the ambiguities in compiler implementations, which C poetically calls _undefined behavior_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.0 History\n",
    "\n",
    "As with C, C++ came to be at AT&T / Bell Labs, initially as a transpiler (pre-compiler) to C, much like its JavaScript counterparts of today. \\\n",
    "Eventually, it became its own programming language with a standalone compiler. The end.\n",
    "\n",
    "This is as much as I want to delve into its history at this time. Read the wiki for the rest."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1. File names and compilation\n",
    "\n",
    "Its source files use the following extensions:\n",
    "- `cc`\n",
    "- `cpp`\n",
    "- `cxx`\n",
    "\n",
    "The headers of the standard C++ library have no extension, other projects use\n",
    "- `hh`\n",
    "- `hpp`\n",
    "- `hxx`\n",
    "\n",
    "While the C compiler on UNIX has a de-facto invocation (symlink) of `cc`, C++ compiler usually needs to be called with its actual name, for gcc this is `g++`, for Clang it's `clang++`.\n",
    "\n",
    "Or if you're really stubborn, since compiler frontends of today handle both of these languages, you can still use `cc` and specify C++ with `cc -x c++`.\n",
    "\n",
    "C program\n",
    "```sh\n",
    "cc test.c -o test\n",
    "gcc test.c -o test\n",
    "clang test.c -o test\n",
    "```\n",
    "\n",
    "C++ program\n",
    "```sh\n",
    "cc -x c++ test.c -o test\n",
    "g++ test.c -o test\n",
    "clang++ test.c -o test\n",
    "```\n",
    "\n",
    "[More info](https://stackoverflow.com/questions/9148488/how-do-i-compile-c-with-clang)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2. Differences compared to C\n",
    "\n",
    "### Literal handling\n",
    "\n",
    "The `sizeof` operator determines the number of *bytes* required for storing the resulting type.\n",
    "\n",
    "C converts all literals into `int` types, thus `sizeof(1) == sizeof('a') == 4` (on most 32 / 64 bit systems).\n",
    "\n",
    "C++ retains the type for as long as possible, thus\n",
    "- `sizeof(1) == 4`\n",
    "- `sizeof('a') == 1`\n",
    "\n",
    "Implicit conversions that would cause loss of precision / truncation are reported by the C++ compiler."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function prototypes\n",
    "\n",
    "In C, skipping function arguments (without explicit `void`) provides the compiler with an incomplete definition (the argument list is not prototyped) which matches with all of its declarations, regardless of the argument list of the actual declaration.\n",
    "\n",
    "When calling a function via its incomplete definition, all provided arguments are first promoted, then passed according to the calling convention.\n",
    "\n",
    "```c\n",
    "/* The following will compile without warnings even with -Wall. */\n",
    "\n",
    "/* Note: explicitly using void / specifying arguments will avoid this issue */\n",
    "int a();\n",
    "\n",
    "int main() {\n",
    "\n",
    "    a(1);\n",
    "    a(1,2,3);\n",
    "\n",
    "    return 0;\n",
    "}\n",
    "\n",
    "/* Note that using other types as int will actually cause the gcc compiler to check provided arguments and will throw an error (as it should) */\n",
    "int a(int b) {\n",
    "    return 1;\n",
    "}\n",
    "```\n",
    "\n",
    "C++ treats the absence of the argument list as an implicit `void`, thus calling the function with arguments will catch the error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### `main` function definition\n",
    "\n",
    "As far as the linker is concerned, its only requirement is that you provide something called `main`, and the runtime will try to call it.\n",
    "\n",
    "The standard C runtime provides it with arguments (`int argc, char **argv, char **envp`) and expects an `int` return value, however it has no means to enforce this. It merely *hopes* that the memory contents, starting at the `main` symbol, represent code that follows the definition and uses a compatible [calling convention](https://en.wikipedia.org/wiki/X86_calling_conventions).\n",
    "\n",
    "Nowadays, the compiler will try very hard to prevent (or at least warn) you from writing a non-compliant `main` function, but if you feel especially evil\n",
    "```sh\n",
    "echo | gcc -x c - -c -o test.o \n",
    "objcopy --add-symbol main=0 test.o\n",
    "gcc test.o -o test\n",
    "\n",
    "# Calling the resulting ./test executable obviously segfaults, but as noted,\n",
    "# the linker itself does not care about the type or validity of the main symbol\n",
    "```\n",
    "\n",
    "The C++ compiler and runtime retained the definition, with the following notable differences:\n",
    "- return type must be `int`\n",
    "- `argv[argc]` is guaranteed to be `NULL` (this wasn't standard, but usually the case for most C runtimes)\n",
    "- the `char **envp` parameter has been deprecated and the environment should be accessed via `extern char **environ` instead\n",
    "\n",
    "And (as set by standard) the C++ `main` gets some additional special treatment\n",
    "- skipping `return` will implicitly return 0 (and won't emit a warning)\n",
    "- its symbol name [does not get mangled](https://stackoverflow.com/questions/53393585/why-name-mangling-has-no-effect-on-main-function-in-c), which in turn prevents it from being overloaded\n",
    "\n",
    "Also see [the standard](https://eel.is/c++draft/basic.start.main) for more info.\n",
    "\n",
    "In terms of exiting the program, in C++ you should strive to return from main in order to call destructors of local variables (automatic storage duration, allocated on stack) if the destructors do any significant work besides de-allocation (eg. ending comm session, data flushing).\n",
    "\n",
    "If that's not an issue, you might use `std::exit` - note that [the C variant `exit` is deprecated](https://stackoverflow.com/questions/38724788/what-is-the-difference-between-exit-and-stdexit-in-c)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strict type checking\n",
    "\n",
    "Both C and C++ don't allow referencing undeclared variables.\n",
    "\n",
    "However, in C, you can refer to / call an undefined function (because, technically, this is linkers problem), and, most of the time, things will work.\n",
    "\n",
    "```c\n",
    "/* Not including stdio.h */\n",
    "int main(void) {\n",
    "    printf(\"0x%08X, %f\\n\", printf, 4.0);\n",
    "    return 0;\n",
    "}\n",
    "```\n",
    "\n",
    "Though, in this case, you're putting yourself at the mercy of implicit type conversions and the calling convention (which depends on the architecture, OS, compiler, etc.)\n",
    "```c\n",
    "#include <stdio.h>\n",
    "\n",
    "//void test(int a, int b, int c);\n",
    "\n",
    "int main(void) {\n",
    "\n",
    "    /* Referenced before being declared */\n",
    "    /* Definition is being implicitly inferred from argument types and passed according to the calling convention */\n",
    "    test(1, 2.0, 3);\n",
    "\n",
    "    return 0;\n",
    "}\n",
    "\n",
    "void test(int a, int b, int c) {\n",
    "    printf(\"%d %d %d\\n\", a, b, c);\n",
    "}\n",
    "```\n",
    "\n",
    "C++ does not allow for that (error instead of warning), all function and variable references must thus first be declared.\n",
    "\n",
    "Additionally, implicit conversions of void pointers (a much abused feature of C) is not allowed in C++.\n",
    "```c\n",
    "int main() {\n",
    "    \n",
    "    const void *a = \"a\";    // OK\n",
    "    const char *b = a;      // OK in C, NOT in C++\n",
    "\n",
    "    return 0;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function overloading\n",
    "\n",
    "While arguably part of OOP, the concept of [polymorphism](https://en.wikipedia.org/wiki/Polymorphism_(computer_science)) can be leveraged in C++ without defining classes.\n",
    "\n",
    "C++ allows for multiple functions of the same name, and the caller selects the one with the matching argument count, type - a concept called function overloading.\n",
    "\n",
    "In order to be compatible with existing linkers, the names of all functions (except main) are mangled (get pre/postfixed according to return/argument types), so that overloaded functions get unique symbols.\n",
    "\n",
    "While compiler-specific, it is standardized with the [Application Binary Interface (ABI) specification](https://gcc.gnu.org/onlinedocs/libstdc++/manual/abi.html), most use the [Itanium C++ ABI](http://itanium-cxx-abi.github.io/cxx-abi/).\n",
    "\n",
    "Even though the return value determines the mangled name, the C++ compiler does not allow polymorphism based just on the return value (some esoteric languages, like [VHDL](https://en.wikipedia.org/wiki/VHDL), allow that).\n",
    "\n",
    "It does, however allow for polymorphism based on `const`ness of arguments - which allow separate function for handling const and non-const objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### Default arguments\n",
    "\n",
    "For people coming from Pascal, Python and the likes, they might be delighted to see that C++ supports default arguments. However, they come with their caveats (and no named / keyword arguments, har har).\n",
    "\n",
    "When defining / declaring a function, a default value can be provided at argument declaration. This value is then filled in by the compiler if it's omitted at the call."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void myfunc(const char *str = \"Default\") {\n",
    "    std::cout << str;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Test"
     ]
    }
   ],
   "source": [
    "myfunc(\"Test\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Default"
     ]
    }
   ],
   "source": [
    "myfunc();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that C++ prevents you from defining arguments both at declaration _and_ definition.\n",
    "\n",
    "At the same time, since most function calls are made according to the declaration present in the header file, it's a very bad idea to define default arguments at the definition (even though it might make the most sense semantically), because the compiler will only use the values present in the declaration (in the header file)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Something else"
     ]
    }
   ],
   "source": [
    "// Funnily enough, the Cling interpreter allows redeclaring functions with different default arguments\n",
    "void myfunc(const char *str = \"Something else\");\n",
    "myfunc();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Macros and NULL\n",
    "\n",
    "C++ provides language features that can replace most of the heavyweight work traditionally done with preprocessor macros:\n",
    "- templates (generic programming) and the standard template library for common data structures\n",
    "- constexpr (compile-time evaluation)\n",
    "- various syntactic sugar for easier access and manipulation of objects\n",
    "all while ensuring type safety and (reasonably sane) error reporting.\n",
    "\n",
    "Additionally, preprocessor defines [should also be replaced with `static const` constants](https://softwareengineering.stackexchange.com/questions/142475/how-does-const-storage-work-item-2-scott-myers-effective-c) - practically all C / C++ compilers are able to inline the constant where it makes sense.\n",
    "\n",
    "Using typed constants instead of defines (which are usually simple numeric literals) is even more important in C++ because the type can determine the invoked functionality (function overloading, operator overloading, pointer arithmetic).\n",
    "\n",
    "Therefore, the ubiquitous `NULL` define, used to represent invalid pointers / end of data, and typically defined as a literal `0` (`int` type), should be replaced by [`nullptr`](https://en.cppreference.com/w/cpp/language/nullptr) (which is of type `std::nullptr_t`)\n",
    "\n",
    "Also, while we're talking about the preprocessor, the usual include guards can be replaced with [`#pragma once`](https://en.wikipedia.org/wiki/Pragma_once) *preprocessor* directive in most cases - it's non-standard, but supported virtually everywhere."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Local variables\n",
    "\n",
    "By now it's common sense that we should\n",
    "- keep global variables to a minimum,\n",
    "- encourage locality and\n",
    "- refrain from variable reuse (the compiler will do a better job)\n",
    "\n",
    "There are some less known language constructs that come handy here:\n",
    "- both C and C++ allow self-standing compound statements\n",
    "  ```c\n",
    "  ...\n",
    "\n",
    "  {\n",
    "      int myvar; // limited to this block\n",
    "  }\n",
    "  ...\n",
    "  ```\n",
    "- in C++ (but not in C) we can additionally specify variables in condition/selection clauses of control statements (`if-else`, `switch`, `while`)\n",
    "  ```cpp\n",
    "  while (void *ptr = next()) { // Traverse until nullptr\n",
    "      ...\n",
    "  }\n",
    "  ```\n",
    "  Note that you still can't define variables within nested expressions, so something in the likes of\n",
    "  ```cpp\n",
    "  while ((int c = getchar()) != EOF) { // Nope\n",
    "      ...\n",
    "  }\n",
    "  ```\n",
    "  still isn't possible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Typedef\n",
    "\n",
    "In C we often times we want to reduce the clutter by `typedef`in the type declaration\n",
    "```c\n",
    "typedef struct mystruct_tag {\n",
    "    ...\n",
    "} mystruct_t; // Provides a type `mystruct_t`, which is an alias for `struct mystrict_tag`. The tag may be omitted (anonymous struct).\n",
    "```\n",
    "\n",
    "In C++, tags represent the type, so an additional typedef isn't necessary (but is kept for compatibility)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "struct test {\n",
    "    int a;\n",
    "    int b;\n",
    "};\n",
    "\n",
    "sizeof(test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that\n",
    "- `struct a {};` represents a type `a`, of which multiple instances can be created `a myinst;`\n",
    "- `struct {} a;` represents a (single) instance `a` of an anonymous struct type"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluation order of operands\n",
    "\n",
    "While C has a well defined binding order [(operator precedence)](https://en.cppreference.com/w/c/language/operator_precedence), which is derived from grammar, the standard does not specify the evaluation order of operands of same precedence (only the order of `&&`, ` ||` short-circuit operators were defined), which can present an issue if the evaluation of operands has side effects.\n",
    "\n",
    "C++ standard provided rules for evaluation order:\n",
    "- expressions using postifx operators (index operator `a[b]`, member selector `a.b`) are evaluated LEFT -> RIGHT\n",
    "- operands of shift operators `a << b` are evaluated LEFT -> RIGHT\n",
    "- assignment expressions `a = b` are evaluated RIGHT -> LEFT\n",
    "\n",
    "The following example shows evaluation order via identifier names\n",
    "```c\n",
    "no1.no2\n",
    "no4 += no3 = no2 -= no1\n",
    "no1 << no2 << no3 << no4\n",
    "no1 >> no2 >> no3 >> no4\n",
    "```\n",
    "\n",
    "Overloaded operator retain the same evaluation order as the built-in operators."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3. C / C++ interface\n",
    "\n",
    "While C++ adds on entirely new concepts, it remains ABI compatible (the primitive data types, structures and the calling convention remains the same), and can therefore reuse (possibly pre-compiled) C libraries (and vice versa) through a very thin [FFI (Foreign Function Interface)](https://en.wikipedia.org/wiki/Foreign_function_interface)\n",
    "\n",
    "> The declarations of functions originating from C object files / libraries must be specified with `extern \"C\"` [linkage specification](https://en.cppreference.com/w/cpp/language/language_linkage), which instructs the C++ compiler to skip name mangling when referencing the function.\n",
    "\n",
    "```cpp\n",
    "// The specificetion can be provided as a prefix\n",
    "extern \"C\" void myfunc();\n",
    "\n",
    "// Or as a block, in which we provide multiple declarations\n",
    "extern \"C\" {\n",
    "    void myfunc();\n",
    "\n",
    "    // One can even import a whole C header here\n",
    "    #include \"mylib.h\"\n",
    "}\n",
    "```\n",
    "\n",
    "For the other way around, that is, to link and call C++ libraries from C programs, the C++ compiler provides the `__cplusplus` preprocessor definition that allows us to conditionally compile C++ specific sections (excluding them when compiled in a C program).\n",
    "\n",
    "```c\n",
    "#ifdef __cplusplus\n",
    "extern \"C\" {\n",
    "#endif\n",
    "\n",
    "    void mycppfunc(); // C++ library function, compiled with C linkage (without mangling)\n",
    "\n",
    "#ifdef __cplusplus\n",
    "}\n",
    "#endif\n",
    "```\n",
    "\n",
    "This allows us to use same header files for both languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. The OOP concept\n",
    "\n",
    "_See also [Annotations](http://www.icce.rug.nl/documents/cplusplus/cplusplus02.html#l11)._"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The gist of it\n",
    "\n",
    "Traditionally, we think of a problem as a set of tasks that need to be done in order to achieve results - this is the *procodural approach*. \n",
    "  \n",
    "  > The procedure to solving the problem is decomposed into subtasks until each can be treated as a singular operation on a set of inputs, returning a result - this can be mapped to functions, the interface between them being their arguments and return values.\n",
    "\n",
    "Putting the various nuances of storage allocation and linkage aside, this is as far as C goes.\n",
    "\n",
    "Building on this approach, a problem usually operates on a well-defined data set with well-defined operations that can be performed on it. The *object oriented approach* binds the data and its operations together, treating it as an *object* which can be given a *name / keyword*.\n",
    "\n",
    "  > Further, the same concept of decomposition, used on operations, can be applied to data, decomposing larger datasets into smaller, more manageable, components.\n",
    "  > \n",
    "  > Functinality implemented on these small components can then be reused when combining / extending them into larger ones via inheritance.\n",
    "\n",
    "The definition of a data set along with its functionality is called a *class*, its instance (actual data) an *object*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## OOP Design Patterns\n",
    "\n",
    "As with any powerful tool, it's usage does not make software better by default - it should be applied in moderation and in ways that make sense.\n",
    "\n",
    "Over time, certain approaches to using OOP concepts have formed, serving as guidelines for structuring data in common problems.\n",
    "\n",
    "The initial influential work was [Design Patterns: Elements of Reusable Object-Oriented Software (1994)](https://en.wikipedia.org/wiki/Design_Patterns). A lot of its concepts have been further scrutinized and developed.\n",
    "\n",
    "At this point, this is beyond the introductory discussion of C++, but is an important part of efficiently using the language.\n",
    "\n",
    "This will be explored in a separate notebook, but for the impatient, I suggest the following:\n",
    "- https://refactoring.guru/design-patterns\n",
    "- https://refactoring.guru/design-patterns/cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structs\n",
    "\n",
    "In C++, a `struct` is a class whose members are public by default.\n",
    "\n",
    "They _can_ be used the same way as in C, but they also provide OOP extensions:\n",
    "- they can \"contain\" functions, thereby binding the data manipulation functions to its data\n",
    "- they support inheritance - just be aware that other languages like C# [have their quirks](https://stackoverflow.com/questions/2310103/why-c-sharp-structs-cannot-be-inherited) with them, so this is not a universal concept"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "struct Point {\n",
    "    int x;\n",
    "    int y;\n",
    "\n",
    "    void show() {\n",
    "        // A class member function (method) can directly access its members\n",
    "        std::cout << x << \" \" << y << std::endl;\n",
    "    }\n",
    "};\n",
    "\n",
    "struct Circle {\n",
    "    int x;\n",
    "    int y;\n",
    "    int r;\n",
    "\n",
    "    void show() {\n",
    "        std::cout << x << \" \" << y << \" \" << r << std::endl;\n",
    "    }\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1 2\n",
      "4 5 6\n"
     ]
    }
   ],
   "source": [
    "Point p = {1, 2};\n",
    "Circle c = {4, 5, 6};\n",
    "\n",
    "// Struct / class member functions (methods) are accessed via the dot `.` / arrow `->` operators\n",
    "p.show();\n",
    "c.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also split struct / class declaration and member function definition\n",
    "```cpp\n",
    "struct AnotherPoint {\n",
    "    int x;\n",
    "    int y;\n",
    "\n",
    "    void show();\n",
    "};\n",
    "\n",
    "// We use the scope resolution operator `::` to reference the `show` function within a particular scope (`AnotherPoint` struct)\n",
    "// Note that C++ has multiple different scopes, but we'll get to that\n",
    "void AnotherPoint::show() {\n",
    "    std::cout << x << \" \" << y;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that function declarations are not stored in the `struct` object, only data members (properties) are.\n",
    "\n",
    "Note that, while in trivial cases, C++ structs _usually_ have compatible alignment / padding with its C counterparts, the C++ standard [does not even guarantee the same order](https://stackoverflow.com/a/6185547) (and thus, same alignment, padding) of members, especially when C++ features are being used (access specifiers might reorder members etc.).\n",
    "\n",
    "[Additional overview on member reordering](https://www.embedded.com/access-specifiers-and-class-member-allocation-order/)\n",
    "\n",
    "Additionally, structs with no data members (not a common sight in C, but possible, and happens in C++ if a class only contains functions) [get additional padding](https://stackoverflow.com/questions/2362097/why-is-the-size-of-an-empty-class-in-c-not-zero).\n",
    "\n",
    "```cpp\n",
    "struct A {\n",
    "    // Functions not stored within class, so it's technically an empty struct\n",
    "    // C standard leaves this undefined (older versions of GCC produced a zero size type, newer follow C++)\n",
    "    // C++ standard does not allow zero-sized objects, so most compiler implementations provide 1 byte of padding\n",
    "    void abc();\n",
    "};\n",
    "\n",
    "struct B {\n",
    "    // 1 byte data member, no padding\n",
    "    char abc;\n",
    "};\n",
    "\n",
    "struct C {\n",
    "    // 1 byte data member, 1 member function (does not consume space)\n",
    "    char abc;\n",
    "    void def();\n",
    "};\n",
    "\n",
    "struct D {\n",
    "    // Virtual function, adds a virtual table pointer\n",
    "    // https://stackoverflow.com/a/9439300\n",
    "    virtual void abc();\n",
    "};\n",
    "\n",
    "struct E {\n",
    "    // Static members are not allocated within instances, so they don't contribute to its size\n",
    "    // Technically an empty struct, 1 padding byte\n",
    "    static int x;\n",
    "};\n",
    "```\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++11 (xeus)",
   "language": "C++11",
   "name": "xcpp11"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "C++11",
   "version": "11"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
